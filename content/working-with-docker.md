---
title: "Working with Docker"
date: "2021-08-01"
syndicate: true
---

Tags: [[linux]]


### Docker on a Server
List of commands:
* List the running containers: `sudo docker ps`
* List all the containers: `sudo docker ps -a`
* Details of a container: `sudo docker inspect <container_name>`
* List all the images: `sudo docker images`
* Create a volume to store data into persistent storage (so that even if you remove a container, you don't lose the data) and map it to the container's storage: `sudo docker volume create <volume_name>`

* * *

Before removing any containers, first stop them. Then they can be removed by `sudo docker rm <container_name>`. Following it up, if you want to delete the image as well, run `sudo docker rmi <image_name>`. Keep in mind that images are stored and then on top of that, a container is built and run. So multiple containers can be simultaneously running on a single image (like multiple web-servers)

* * *
### Execute commands in the container

* If you want to execute some commands into the docker container, use the following command:
`docker exec -it <container_id/container_name> <command>`

* So for example, if we want to analyze the tables in a postgres database, follow the steps below:
	* `docker exec -it <container_id> psql -U <dbUsername> <dbName>`
	* Use the `\list` command or its meta-command `\l` to list all the databases
	* To jump to a different database, use `\connect <dbName>` or `\c <dbName>`
	* To list all the tables in that database, use `\dt`
	* You can drop a table using the following syntax: `drop table <tableName>;`

### Use docker without sudo

You need to add your user (which has root privileges) to the docker group by giving the following command:
`sudo usermod -aG docker opc` where opc is my username

After this, logout and do a login and it should work.

* * *

### Building your own image and pushing it to docker hub

* Create a Dockerfile
* Build the docker image using `docker build -t username/imagename:latest .` (Don't forget the `.` at the end, this builds the image using the current directory's Dockerfile)
* Do `docker login`
* Finally push the image to docker-hub using `docker push username/imagename:latest`

* * *

### docker-compose

* `docker-compose` is used if we want to use multiple containers together.
* First create a `docker-compose.yaml` file which defines the services and networks and volumes.
* See the version compatibility here: https://docs.docker.com/compose/compose-file/
* After creating this file, check that you have mentioned everything in a proper format by giving the following command: `docker-compose config`. This will let you know whether you have any errors in the configuration file.
* Start the container: `docker-compose -f docker-compose.yml up -d`

### Freshrss using docker

* First stop the current container: `docker stop freshrss`
* Rename it: `docker rename freshrss freshrss_old`
* Run the following command: `docker run -d --restart unless-stopped --log-opt max-size=10m -v freshrss-data:/var/www/FreshRSS/data -v freshrss-extensions:/var/www/FreshRSS/extensions -e 'CRON_MIN=4,34' -e TZ=Asia/Kolkata --net freshrss-network -p 1337:80 --name freshrss freshrss/freshrss`
* It's much better if you use a docker-compose file to run this. Much cleaner.
