---
title: Doughnut Economics by Kate Raworth
date: "2021-07-22"
syndicate: true
---

Tags: [[books]]

# Scribbled Notes

The Doughnut:
* Outer layer - Ecological ceiling
* Inner layer - Social foundation.

Zones beyond ecological ceiling:
* Climate change
* Ocean acidification
* Chemical pollution
* Nitrogen and phosphorus loading
* Freshwater withdrawals
* Land conversion
* Biodiversity loss
* Air pollution
* Ozone layer depletion

Zones inside social foundation (Absolute necessaties for a common man):
* Water
* Food
* Health
* Education
* Income and Work
* Peace and Justice
* Political voice
* Social equity
* Gender equality
* Housing
* Networks
* Energy

### Seven rules of thinking like a 21st century economist

1) Change the goal from GDP to something oriented towards holistic development while being ecologically sensitive to the impact we're creating in the environment. Study Doughnut methodology.

2) See the big picture: Stop thinking in terms of self-contained markets and start thinking it as an embedded economy

3) Nurture human nature: Step away from the rational economic man and move towards socially adaptable humans

4) Think in terms of systems: Stop considering the economy as mechanical equilibrium and start thinking it as dynamically complex

5) Design to distribute: from 'growth will even it up again' to distributive by design

6) Create to regenerate: from 'growth will even it up again' to regenerative by design

7) Be agnostic about growth: from growth addicted to growth agnostic
