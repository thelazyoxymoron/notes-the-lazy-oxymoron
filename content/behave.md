---
title: Behave by Robert Sapolsky
date: "2021-07-21"
syndicate: true
---

Tags: [[books]] 

## Scribbled Notes

* Can you ever separate doing good from the expectation of reciprocity, public acclaim, self-esteem, or the promise of paradise?
  * Relevant story in New Yorker - The Kindest Cut by Larissa MacFarquhar

* We don't hate aggression; we hate the wrong kind of aggression but love it in the right context.

* Brain's three functional domains - modeled by neuroscientist Paul Maclean. They are as follows:

  * Layer 1: ancient part, mediates automatic, regulatory functions.
  * Layer 2: being about emotions, senses gruesomeness, terrifying and sadness
  * Layer 3: Cognition, memory storage, philosophy, contemplation.

All the 3 layers are not so categorical, they have a considerable overlap too. But it provides a basic crude model to understand the brain.

* Hypothalamus - how layer 2 and layer 1 talk to each other. Frontal cortex - the interface between layer 2 and layer 3

* Autonomic nervous system - 1) Sympathetic Nervous System (SNS) and 2) Parasympathetic Nervous System (PNS)

* SNS - mediates four F's (fear, fight, flight, sex :P). PNS- calm, vegetative states. Normally they both are in opposition in their functionalities.

* Amygdala is responsible primarily for aggression, fear, and anxiety. Insular cortex - the feeling of disgust.

* The frontal cortex makes you do the harder thing when it's the right thing to do. it is the last brain region to fully mature, with the most evolutionary recent subparts the very last. Amazingly, it's not fully online until people are in their mid-twenties.

* Frontal neurons are expensive cells, and expensive cells are vulnerable cells. Relevant - the concept of cognitive load. Make the frontal cortex work hard - a tough working-memory task, regulating social behavior, or making numerous decision while shopping - immediately afterward performance on a different frontally dependant task declines.

* The Pre-frontal cortex - divided into dlPFC(deliberate, think about doing the harder thing) and vmPFC (very emotional). This is the classic debate b/w cognition and emotion, and the reality is we need both to function like a normal human being.

* Dopamine release is more about the anticipation of reward than the reward itself. It is not about the happiness of the reward. It's about the happiness of pursuit of reward that has a decent chance of occurring.

* A norm violation increasing the odds of that same norm being violated is a conscious process. People litter more when there's already a littering, crimes are more when there are more broken windows etc.

* Eyes carry a lot of information about one's emotional states.

* In the moments just before we decide upon some of our most consequential acts, we are less rational and autonomous decision makers than we like to think.

* Another study suggests that oxytocin unconsciously strengthens the pair bond. Heterosexual male volunteers, with or without an oxytocin spritz, interacted with an attractive female researcher, doing some nonsense task. Among men in stable relationships, oxytocin increased their distance from the woman an average of four to six inches. Single guys, no effect. Moreover, oxytocin caused males in relationships to spend less time looking at pictures of attractive women. Importantly, oxytocin didn't make men rate these women as less attractive; they were simply less interested.

* Sometimes oxytocin and vasopressin make us more prosocial, but sometimes they make us more avid and accurate social information gatherer.

* The later a particular brain region matures (looking at you, Pre Frontal Cortex) the less it is shaped by genes and the more by environment (or experience).

* The difference between strongly inherited trait, and high heritability of trait.

* A very interesting difference between individualist culture and Collectivist culture: rice/collectivism connection. It requires collective farming.

* Individual selection is a more appropriate name for what happens in evolution than group selection. And hence, reproduction is the key of evolution - not survival of the fittest.

* The 3 foundations of thinking about the evolution of behavior - individual selection, kin selection, and reciprocal altruism.

* Stereotype content model regarding Us/Them mechanism:

There are two independent axes - "warmth" meaning is the individual or group a friend or a foe, benevolent or malevolent and "competence" meaning how effectively can the individual or group carry out their intentions.

These make four corners of our matrix:

- High warmth/High competence (HH): Us
- High warmth/Low competence (HL): elder people, mentally disabled, or handicaps
- Low warmth/High competence (LH): how the developing world views their colonizers
- Low warmth/Low competence (LL): a homeless person

Some consistent feeling:
HH - pride, LH - envy, HL - pity, LL - disgust.

Needless to say, things are not that straight forward and the categorizations are not this consistent, but it provides a broad understanding.

* Right wing authoritarianism - related to low intelligence. We become more conservative when there's a significant cognitive load (e.g. Time pressure)

* Mirror neurons: PMC activation when you observe/see someone doing an activity, rather than you doing the activity yourself. Ideally, Pre-motor Cortex should only trigger when there's a movement involved.

* Empathy versus compassion: Feeling empathy more often then not, primes us to feel too passionately which inhibits our tendency to do any good. The better and more effective way is to be compassionate - feel at a distance which prompts you to do good.

* One interesting correlation: We use similar brain circuits, activating the medial orbitofrontal cortex, when contemplating whether an act is moral as when contemplating whether a face is beautiful. In other words - we confuse goodness with beauty.

* The "Macbeth Effect" - We intertwine physical and moral purity when it comes to our own actions. When told to tell a lie or a immoral act done in the past, subjects were more prone to wash their hands or mouths afterwards.

* Evolution is a tinkerer - an improviser.

* Resting potential and action potential: when neurons have nothing to say (negatively charged inside) vs. When neurons have lots of things to say (positively charged)
