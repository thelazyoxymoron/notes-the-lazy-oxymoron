---
title: Debugging Procrastination
date: "2021-07-21"
syndicate: true
---

Tags: [[personal-development]]

### Definition of procrastination?
* Irrational delay in doing things
* Caused by personality traits, cognitive affects, "I perform better when under pressure"
* When the study was conducted with folks saying that they perform better under pressure, no-one said instinctively that they're happy that they pushed off the task for the last minute
* The high that you get after you finish a task in a short amount of time maybe exaggarates the feeling you get of achieving something worthwhile and cause you to overestimate your dependency on stress for optimal performance

### Personality traits
* Conscientiousness (C): self-disciplined, organized, follow through on things - Perhaps the most important trait to consider when studying procrastination
* Agreeableness (A): Altruistic, helpful, easy to be around
* Neuroticism (N): Emotional stability - Probably the most studied
* Openness to experience (O): Intellectually curious
* Extroversion (E)

### Relation of the tendency to procrastinate with personality traits:
* Conscientiousness is the most highly correlated with procrastination amongst the five traits
* Higher the conscientiousness, lower the tendency to procrastinate
* Neroticism - emotional stability - is the 2nd most correlated trait
* More impulsive you are, more prone you are to procrastination
