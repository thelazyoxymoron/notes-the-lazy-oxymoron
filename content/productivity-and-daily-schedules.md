---
title: Productivity and Daily Schedules
date: "2021-07-21"
syndicate: true
---

Tags: [[personal-development]]

I came across an interesting episode in [Huberman Lab](https://hubermanlab.com/maximizing-productivity-physical-and-mental-health-with-daily-tools/) podcast which discussed about the optimal way to structure your daily habits/schedule, to reap maximum benefits.

It largely follows the below structure:
* **Temperature Minimum:** Record your average wake-up time. The temeperature minimum is the time approx. 2 hours before you wake up when your body temperature is at its lowest (in a 24 hour cycle).
	* This is related with your optimal work time, which falls generally about 5-6 hours after your temperature minimum.

* **Forward motion:** Basically take a walk, preferably in the outdoorsy setting. This activates your eyeball sideways movement, which is reported to help increase your attention levels

* **Natural light:** View natural light for 10-30 minutes every morning, walking outdoor helps with this

* **Hydrate:** Hydrate correctly. This is absolutely vital (I agree) 

* **Delay caffeine:** He suggested to delay caffeine to 90-120 minutes after wake up. I'm not sure I agree, would need to experiment more on this (TODO)

* **Fast until noon:** I already do this. Not a strict fast for example, but eat very little in the morning hours. I don't like the feeling of full stomach early in the day.

* **90 minutes work bouts:** Try to do a 90-minute work bout. This doesn't have to be absolutely strict. Post this 90 minute block, try to do some strength training/exercise. (TODO: is morning the best time for this? I prefer to workout in the evening. Need to experiment). Also do workout for at least 5 days a week.
