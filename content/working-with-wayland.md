---
title: "Working with Wayland"
date: "2021-08-01"
syndicate: true
---

Tags: [[linux]]

`wayland` is the new and shiny display server, intent is to replace `x11`

* To work with AMD/ATI video cards, install `mesa` package

* Disable internal keyboard in wayland:
	* since we don't have `xinput` in wayland, the replacement is `libinput`
	* However, there's no straightforward way to manage disabling, the best way I've found is to use  configuration management of `sway`
	* List the devices using `swaymsg -t get_inputs`
	* Note the identifier. For internal keyboard, it would be: `AT Translated Set 2 Keyboard`
	* Use the following in sway's conf file (`~/.config/sway/config`)
			```
			input "<identifier>" {
				events disabled
			}
			```

* Disable internal monitor/screen:
	* get outputs using `swaymsg -t get_outputs`
	* disable in sway's config using `output LVDS-1 disable`

* Use `kitty` terminal.
	* It has a plugin-management system called `kitten` which can do plethora of interesting tasks.
	* For example, to display an image in the same terminal, use `kitty +kitten icat <image.jpg>`
	* You can add an alias into your bashrc to make it easier, use

* To dim your screen, instead of using `redshift`, you can use `gammastep` in wayland. As of 31st May, 2021, there was no wayland support in `redshift`. See this: https://github.com/jonls/redshift/issues/55

* To setup changing wallpapers, use this project. Seems to be quite popular and handy way to handle wallpapers.
	* Original Project: https://github.com/adi1090x/dynamic-wallpaper
	* Fork with sway support: https://github.com/GitGangGuy/dynamic-wallpaper-improved
	* This ideally should be used with `crontab` to change the wallpaper every hour
	* Use `cronie` to achieve that. Instructions in archwiki and linked github project

* Sway has a cool functionality of `scratchpad`, where you can send windows that you don't want right now, possibly because you want to declutter your workspaces.
	* You can use `$mod+shift+minus` to send a window to scratchpad
	* You can cycle through the windows in scratchpad using `$mod+minus` and bring it back into view using `$mod+shift+space`

* While installing `anki` via the default tar method from the official packaged download, it was failing in wayland with the following error:
	* `Failed to load client buffer integration: "wayland-egl"`
	* The workaround (on 01/06/2021 00:09), it was to install via pip in the following fashion: `sudo pip install anki aqt`

* Take screenshots on wayland:
	* Use `slurp`  to capture the region and use `grim` to take screenshot
	* These two can be combined to take screenshot according to the region. Once such tool is: https://gitlab.com/radio_rogal/swayshot

* `udiskie`: if you need to safely eject some drive from the command line, you the following syntax: `udiskie-umount --detach --eject /media/<usb-name>`

* For document viewing, a very good one that I found was `llpp`. Based off of `muPDF` which also supports EPUB and other file formats.
	* Another one that seems pretty good is `foliate`: https://github.com/johnfactotum/foliate
	* This is present in Arch community repo, so you just need to do `sudo pacman -S foliate`
	* One of the optional dependencies for `foliate` was `dictd` for offline dictionary support

* `xdg-desktop-portal-wlr` is needed for screen-sharing to work in Firefox. Check this: https://github.com/emersion/xdg-desktop-portal-wlr
	* Set this in your sway config file: `exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway`

* In wofi (the app launcher), there is currently a bug/limitation where it can only be run if the application has a desktop entry. You can make a relevant desktop entry as follows:
	* copy the dektop entries from existing apps, e.g. Joplin.desktop or Tribler.desktop (see /usr/share/applications/)
	* in `Exec` command, replace the exact command that you'd use to run that app on terminal
	* Install that `<appname>.desktop` into /usr/share/applications/ by running this command: `install -Dm644 <appname>.desktop /usr/share/applications/<appname>.desktop`

* Installing Wireguard in Archlinux:
	* You'd need `wireguard-tools` to setup this
	* Use wg-quick utility to bring up the interface `wg0`
	* Setup `/etc/wireguard/wg0.conf` by following what you have in your mobile/mac setup
	* Make sure that you have `systemd-resolvconf` installed if you're using `systemd-resolved` to work with dns resolution, as wg-quick requires `resolvconf` by default

* Working with Anki on wayland:
	* There were some problems with Anki in wayland, when installation was done via official tar zip (version 2.1.44 or so). See this: https://forums.ankiweb.net/t/anki-doesnt-start-under-wayland-linux/10409
	* The workaround was to use the pip install version as following: `sudo pip install anki aqt`
		* Keep in mind that you need to do this system wise installation if you want your menu launchers to work with Anki
		* Also, if for some reason, like add-on incompatibility, you have to use an older version of anki, make sure that you first downgrade using `File -> Switch Profile -> Downgrade & Quit`
		* Also, you can install a particular version of anki from pip using `sudo pip install anki==2.1.43 aqt==2.1.43`

