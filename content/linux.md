---
title: "Linux"
date: "2021-07-21"
syndicate: false
---

I love building and breaking things in the process. Sometimes this is a vanity exercise where I just completely overhaul my existing system, just because I wanted to and it's fun. Linux is the perfect paradise for tinkerers (or so I was told), and I found it to be every bit as true as was advertised!

My current setup is [[Arch Linux]] + Wayland/Sway and I'm loving it. Some of my customizations/tricks for this setup is documented here: [[working in linux]]
