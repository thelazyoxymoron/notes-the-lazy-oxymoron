---
title: About These Notes
date: "2021-07-21"
syndicate: true
---

I'm in love with [Andy's working notes](https://notes.andymatuschak.org/About_these_notes), so I decided to organise my thoughts in the same manner.
I found this excellent [repo](https://github.com/aravindballa/gatsby-theme-andy) on github trying to emulate Andy's theme, so decided to give it a spin for my notes as well.

## Motivation

I am a firm believer in working in public (Andy talks about this in [work with the garage door open](https://notes.andymatuschak.org/About_these_notes?stackedNotes=z21cgR9K3UcQ5a7yPsj2RUim3oM2TzdBByZu)). I've always gravitated more towards people who are building things in public, sharing their experiences/failures/successes, basically more interested in the process than the finished product.

One downside of doing this is that by it's very nature, the process is messy which brings out the fear of doing your dirty laundry in public. On the other hand, this process is also liberating in some way - you are less concerned about seeking validation which comes hand in hand with only interested in releasing a finished product.

And so beware, this space is always a work in progress. These notes might change significantly as time goes by!

## Glossary

Interacting with these notes might be difficult for a newcomer, so this space would serve as a jump-off point. I've not yet decided on how I'm going to structure this, maybe a tags based glossary or broad categories, but that remains to be seen.

You can access the directory here: [[Glossary]]

Enjoy your stay here and enjoy exploring :)
