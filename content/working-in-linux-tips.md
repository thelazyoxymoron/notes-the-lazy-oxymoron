---
title: "Working in Linux: Tips"
date: "2021-08-01"
aliases: ["working in linux"]
syndicate: true
---

Tags: [[linux]]

# Bash

* If you want to copy everything in the folder except for some files/folders, use `rsync` in the following way:
	* `rsync -av * destination/ --exclude logs/ --exclude this_file_also.md`

# Cmus

* To play a folder, add it to the current queue by typing `e` while on folder view (via 5)
* Clear a queue: `:clear -q`

# llpp

* To do color inversion (for example, reading in dark mode), use `-I`
	* See this: https://github.com/moosotc/llpp/issues/151

# zathura (best viewer for comic books!)
* Install `zathura` and `zathura-cb` package for comic book support
* Use `d` for 2 page view
* `Shift+j` and `Shift+k` for going to next/previous page in the view
* `zi` and `zo` works for zooming in/out

# Vifm

* Shortcut for opening the vifmrc config file and apply it
	* This is default mapped to `,c` where leader is `,`

* Add a bookmark: `:bmark <tag_name>`
	* See existing bookmarks: `:bmarks`

# Vim

### The Basics

Well, not really the basics, but the best way to get started with a powerful vim setup is to close that `awesome-vimrc` project on github.

Once you're done, proceed further.

### Settings

* Use `coc.vim` for autocompletion (much better than YouCompleteMe)
* Install a language server for autocmpletion to work.
	* For Javascript/Typescript, use `:CocInstall coc-tsserver`

* Another great thing to install would be the coc marketplace, which you can install using `:CocInstall coc-marketplace`
	* Afterwards, use the following command to select some plugins to install: `:CocList marketplace`

* You can check for what keys have been mapped with what commands using the following syntax:
	* `:verbose imap <tab>`: this will show whether that `<tab>` key has been mapped with any commands or not.

* **Using NERDTree:**
	* if the mapped leader is `,`, use this to bring up NERDTree window: `,nn`
	* If you need to refresh the NERDTree, just select the directory node and press `r`
	* To create a file/directory from the NERDTree window:
		* First, bring up NERDTree and navigate to the directory where you want to create the new file. Press `m` to bring up the NERDTree Filesystem Menu. This menu allows you to create, rename, and delete files and directories. Type `a` to add a child node and then simply enter the filename. You’re done! To create a directory follow the same steps but append a `/` to the filename.

* **Useful commands:**
	* Close all tabs at once: `:qa`
	* Close all tabs while saving everything: `:wqa`

# Weechat

* Remove the IRC "spam" messages like "bla-bla-bla has joined/left"
	 ```
	/set irc.look.smart_filter on
	/filter add irc_smart * irc_smart_filter *
	/save
	 ```
* Useful shortcuts (all of these taken from here: https://weechat.org/files/doc/stable/weechat_user.en.html#key_bindings)
	* Switch b/w buffers: `Alt+x`
	* Scroll buffer history by a few lines rather than the whole page: `Alt+PgUp/Alt+PgDown`
	* Switch to previous/next buffer: `Ctrl+p/Ctrl+n`. Also `F5/F6`
	* Scroll up/Scroll down in nicklist: `F11/F12`
	* Switch to next buffer with activity: `Alt+a`
	* Toggle Bufferlist (left window): `Alt+Shift+b`
	* Switch to buffer (for 0-9): `Alt+0..9`
	* Switch to buffer by number (for 01..99): `Alt+j+01..99`

# HPI

See this: https://github.com/karlicoss/HPI
See this: https://github.com/karlicoss/promnesia

* One big thing to keep in mind is that rather than installing these libraries from pip, you fork the source code, and install using the local git repo via `pip install --user -e .`
	* This will install the package in editable mode, which means that any changes to this local repo would be immediately reflected without need to reinstall anything.

* When changing any config, you can check for syntax and other issues using `hpi config check`
* You can also check for any specific module using `hpi doctor my.modulename`
* Once you're done with this config, these sources would be available as python module.
* You can also list all available modules using `hpi modules --all`
* Now the next thing would be to define these sources in `Promnesia` in config `.config/promnesia/config.py`
* Having made the config changes, you can check this using `promnesia doctor config`
* Afterwards, run the indexer `promnesia index`
	* This can be run periodically in order to always make the updated data available
	* For updated data, also make sure that you periodically export all of your sources data, probably via cron jobs
	* For example, you can run the indexer every day using crontab
		* `EDITOR=vim crontab -e`
		* `@daily promnesia index > /tmp/promnesia-index.log 2>/tmp/promnesia-index.err`
* Afterwards, if you want, you can quickly check your database using `promnesia doctor database`
	* This would require `sqlitebrowser` to be installed
* Now run the server using `promnesia serve`
	* This can be run as a service using `promnesia install-server`
	* To get the arguments possible with any command, use `promnesia -h` or `promnesia install-server -h`
	* If you're storing the database in a different location (mentioned in promnesia config file), then do `promnesia install-server --db /home/mr_robot/path/to/sqlitedb/promnesia.sqlite`
* Voila! You're done!

# FZF

* It's an amazing command-line utility for making your general day-to-day operations a lot smoother.
* You can install this system-wide via pacman `sudo pacman -S fzf`
* Next thing to do would be incorporate it into your bash. To do that, source the key completion bash files in your bashrc. So do the following in your `~/.bashrc`: `source /usr/share/fzf/key-bindings.bash`
* The most-used shortcut would be `CTRL+T` for fuzzy finding the files in the current directory and sub-directories. `CTRL+R` does reverse search in bash, so that should also be pretty usable in day-to-day work.
* If you want to integrate fzf with vifm, follow this: https://wiki.vifm.info/index.php/How_to_integrate_fzf_for_fuzzy_finding
* The `locate` shortcut was not working on my machine, so I went ahead with `find` only
