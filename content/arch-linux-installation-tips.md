---
title: "Arch Linux Installation: Tips"
date: "2021-07-22"
aliases: ["Arch Linux"]
syndicate: true
---

Tags: [[linux]]

Although the gold standard is to always follow the ArchWiki, there are few things that I always forget when I'm installing a new setup. This note is just a collection of few tips/tricks that I find useful.

# Follow the installation guide of Arch Linux!
https://wiki.archlinux.org/title/Installation_guide

# Keep your pacman package list

`pacman -Qeqn > repo_pkgs.txt; pacman -Qeqm > AUR_packages.txt`

# Creating bootable drive
* Use [ventoy](https://www.ventoy.net/en/index.html)
* After installing ventoy, do `sudo ventoyweb` and then install ventoy on the USB drive
	* Ventoy is a special utility where you just install the ventoy utility on the drive, and then just copy the iso to `ventoy` mounted drive
	* This enables multiple OS installations to be doable via the same drive, at the same time!

# Creating system partition
* If installing from scratch, it's better to do a clean install and create new partitions.
* To do that, use `fdisk /dev/sda`, if sda is your primary device.
* Then delete the partitions and create new ones
* Since UEFI is almost required now (as Intel is phasing out support for other modes), make sure that you create a `EFI system` partition of at least `512 MB`
* Create a new partition in the following order:
	* type `fdisk /dev/sda`
	* type `n`
	* prompt would be partition number, for EFI, use 1 and then subsequent numbers for other partitions
	* First sector, give the first block, e.g. `34` or `2048`
		* One important thing to keep in mind here, if you select the first block i.e. start the sector from 34, then you might get an error saying `partition does not start on physical boundary`
		* Check this: https://askubuntu.com/questions/156994/partition-does-not-start-on-physical-sector-boundary
		* Solution? Start the sector with something divisible by 8, e.g. 64
	* last sector, type how much space do you want for the partition, so for EFI, it would be `+512M`
	* This creates a default partition type of `Linux filesystem`
	* Since for EFI, we don't want this, we'd change the partition type. type `t`, followed by `L`, which lists all the partition types.
	* type the corresponding partition type number, e.g. for `EFI System`, type 1
	* Create other partition for your root filesystem with the default `Linux filesystem` which should be sufficient

* Create filesystems on the partitions
	* for EFI: use `mkfs.fat -F32 /dev/sda1` if sda1 is your EFI partition
		* If you get any nasty SQUASHFS errors, keep your fingers crossed and reboot. It should resolve itself.
	* for root filesystem: use `mkfs.ext4 /dev/sda3`

# Mount partitions and install

* Mount these partitions: `mount /dev/sda3 /mnt`
	* make a directory to store `efi` files
	* `mkdir /mnt/efi`
	* `mount /dev/sda1 /mnt/efi`

* Use `pacstrap` to install the system into your root directory
	* `pacstrap /mnt base linux linux-firmware vim man-db man-pages texinfo`

* Generate `fstab` file: `genfstab -U /mnt >> /mnt/etc/fstab`

* Change root into the new system: `arch-chroot /mnt`

* After setting up the hwclock and localtime, `Locales` are used for rendering text, correctly displaying regional monetary values, time and date formats, and other locale-specific standards.
	* Process is: Generate a locale using `/etc/locale.gen`. Uncomment the ones that you want to keep
	* run `locale-gen`
	* For a list of enabled locales, run `locale -a`
	* To display the currently set locale, run `locale`

* Set your root password with `passwd`
* **Remember that all this time, you're in arch-chroot**

* Installing grub and enable microcode updates (for intel/AMD cpus):
	* `pacman -S grub efibootmgr`: efibootmgr is required for UEFI systems
	* `grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/efi`
	* Generate your grub configuration file: `grub-mkconfig -o /boot/grub/grub.cfg`
	* Install the microcode package for your CPU: `pacman -S intel-ucode`
	* regenerate your grub configuration file

* reboot your system and remove the installation drive. Login with your root account!

# User management

* One key thing to keep in mind is that the arch iso comes pre-installed with iwctl, so you can easily connect to wifi and install packages. However, once you've logged into your new installation and if you haven't explicitly installed any network management tool, then you might face difficulties in installing anything else on the system.
* So the workaround is to `arch-chroot` into your system and install `iwctl` via the bootable drive to make it work.
* Follow the following steps:
	* `iwctl`
	* `device list`
	* `station wlan0 scan`
	* `station wlan0 get-networks`
	* `station wlan0 connect <ssid>`
	* Provide the passphrase and you're good to go.

# Network Management

* Copying network management section from [Useful things learnt](:/6ce5006aa46148da8e65a8418ce3d0bf):
	* **Networking in Arch Linux:**
		* Use `systemd-networkd` and `iwd` for ethernet and wireless network connections respectively, instead of any network manager or `wpa_supplicant` or `netctl`. Make sure that you go through the iwd arch wiki page thoroughly if anything breaks, as `iwd` is still in active development.
		* Instead of using any standalone solutions for dynamic ip allocation like `dhcpcd`, use the in-built configuration of iwd. It's good enough for the task.
		* For DNS resolving, use `systemd-resolved`
		* More details here: https://www.reddit.com/r/archlinux/comments/hq79dj/i_am_getting_this_error_wifimenu_worked_before/

* If you face errors like `temporary failure in name resolution`, that means you haven't yet started/enabled the `systemd-resolved.service`

* Create this file to manually provide which DNS server to use:
	* `/etc/systemd/resolved.conf.d/dns_servers.conf`
	* If you're using Quad9 (which provides good security and privacy policy), keep in mind that it blocks few websites (malicious, malware, exploit kit domains) if DNSSEC is enabled. Keep this in mind if while browsing any webpage, you're unable to connect.

* Use `resolvectl status` see the DNS currently in use by `systemd-resolved`

* Use `resolvectl query <website>` to query DNS records

# Bluetooth

* Found blueman to be clunky. Others had too many dependencies, so settled on `blueberry`. This is already in pacman databases.
	* To work with this, we first need to start the bluetooth service via `sudo systemctl start bluetooth.service`
	* Otherwise, we need to add blueberry to sudoers/add to the `rfkill` group, which is not really recommended.
