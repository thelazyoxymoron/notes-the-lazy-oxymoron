/** @jsx jsx */
import React from 'react';
import { Styled, jsx, Box } from 'theme-ui';

import ReferredBlock from './ReferredBlock';

const Footer = ({ references }) => {
  return (
    <Box p={3} sx={{ borderRadius: 2 }} mb={2} bg="accent" color="text-light">
      <ReferredBlock references={references} />
      <p sx={{ m: 0, fontSize: 1 }}>
        If this note resonated with you, be it positive or negative, send me a{' '}
        <Styled.a
          sx={{ textDecoration: 'underline', color: 'text-light' }}
          href="https://twitter.com/thelazyoxymoron"
        >
          direct message
        </Styled.a>{' '}
        on Twitter or an{' '}
        <Styled.a sx={{ textDecoration: 'underline', color: 'text-light' }} href="mailto:sid@thelazyoxymoron.me">
          email
        </Styled.a>{' '}
        and we can discuss more.
      </p>
    </Box>
  );
};

export default Footer;
