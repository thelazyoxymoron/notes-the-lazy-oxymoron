module.exports = {
  siteMetadata: {
    title: "Siddhartha's Second Brain",
    siteUrl: 'https://notes.thelazyoxymoron.me',
  },
  plugins: [
    {
      resolve: 'gatsby-theme-andy',
      options: {
        rootPath: '/',
        rootNote: 'about-these-notes',
        linkifyHashtags: true,
        hideDoubleBrackets: true,
        generateRSS: true,
        rssPath: '/rss.xml',
        rssTitle: 'My notes'
      },
    },
  ],
};
